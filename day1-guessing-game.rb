STDOUT.sync = true

def get_int prompt="\nNumber: "
    until !(print prompt) and !(m = /^[+-]?\d+$/.match(gets.strip)).nil?
        puts 'That is not an integer!  Please try again.'
    end
    return m.string.to_i
end

puts 'Welcome to the guessing game!'
answer = rand(101) # 0-100

until guess = get_int("\nYour guess: ") and guess == answer
    puts "Wrong!  It is #{answer > guess ? 'higher' : 'lower'}..."
end

puts "Correct!  The answer was #{answer}"
