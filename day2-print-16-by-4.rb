puts "\njust with each..."

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]

a.each do |e|
    i = a.index e

    if i % 4 == 0
        print a[i], ' '
        print a[i + 1], ' '
        print a[i + 2], ' '
        puts a[i + 3]
    end
end

puts "\nnow with slices..."

a.each_slice(4) {|a, b, c, d| print a, ' ', b, ' ', c, ' ', d, "\n"}

puts "\nlooks the same!\n\n"
