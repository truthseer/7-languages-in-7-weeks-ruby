module ActsAsCsv
    def self.included base
        base.extend ClassMethods
    end

    module ClassMethods
        def acts_as_csv
            include InstanceMethods
        end
    end

    module InstanceMethods
        attr_accessor :headers, :csv_contents

        def initialize
            read
        end

        def read
            @csv_contents = []
            filename = self.class.to_s.downcase + '.csv'
            file = File.new filename
            @headers = file.gets.chomp.split(', ')

            file.each do |row|
                @csv_contents << row.chomp.split(', ')
            end
        end

        def each
            @csv_contents.each do |row|
                yield CsvRow.new Hash[*@headers.zip(row).flatten]
            end
        end
    end

    class CsvRow
        def initialize hash
            @hash = hash
        end

        def method_missing name, *args
            @hash[name.to_s]
        end
    end
end

class RubyCsv # no inheritance! You can mix it in
    include ActsAsCsv
    acts_as_csv
end

m = RubyCsv.new
puts m.headers.inspect
puts m.csv_contents.inspect
puts

m.each {|row| puts row.fname}
