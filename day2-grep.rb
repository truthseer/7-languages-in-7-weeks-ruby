def grep file, match
    r = Regexp.new match
    file.each {|s| (print (file.pos + 1), ' ', s) unless r.match(s).nil?}
end

File.open('day2-grep.rb') {|f| grep f, 'Regexp'}
