class Tree
    attr_accessor :children, :node_name

    def initialize(name, children=[])
        if name.class == Hash
            @node_name, children = name.first

            if children.class == Hash
                h_to_t children
            else
                @children = children
            end

        elsif children.class == Hash
            @node_name = name
            h_to_t children
        else
            @node_name = name
            @children = children
        end
    end

    def h_to_t hash
        @children = []

        hash.each_key do |k|
            @children.push Tree.new(k.to_s, hash[k])
        end
    end

    def visit_all &block
        visit &block
        children.each {|c| c.visit_all &block}
    end

    def visit &block
        block.call self
    end
end

tree1 = Tree.new('Ruby',
                 [Tree.new('Reia'),
                     Tree.new('MacRuby')])
tree2 = Tree.new({'grandpa'=>{
    'dad'=>{'child 1'=>{}, 'child 2'=>{}},
    'uncle'=>{'child 3'=>{}, 'child 4'=>{}}}})

[tree1, tree2].each do |tree|
    puts "\n--------------\n\nVisiting a node...\n\n"
    tree.visit {|node| puts node.node_name}

    puts "\n\nVisiting entire tree...\n\n"
    tree.visit_all {|node| puts node.node_name}
end
